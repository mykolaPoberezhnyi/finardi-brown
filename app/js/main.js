$(document).ready(function() {
    // Плавная прокрутка к блоку
    $("body").on("click", ".link", function(event) {
        event.preventDefault();
        if ($(this).hasClass('link')) {
            var id = $(this).attr('href');
            var top = $(id).offset().top;
            $('body,html').animate({ scrollTop: top }, 1000);
        }
    });
    $("body").on("click", ".js-modal-privacy", function(event) {
        event.preventDefault();
        openModalPrivacy();
    });
    $('.review-carousel').owlCarousel({
        items: 2,
        loop: false,
        nav: true,
        dots: true,
        navText: ['',''],
        responsive : {
            0 : {
                margin: 0,
                items: 1
            },
            768 : {
                margin: 30,
                items: 2
            },
            992 : {
                margin: 80,
                items: 2
            },
            1200 : {
                margin: 120,
                items: 2
            }
        }
    });
    $('.phone>input').mask("+7(999) 999 99 99");
    // var map = new ymaps.Map("map", {
    //     center: [55.76, 37.64], 
    //     zoom: 7
    // });

    // select2
    $('.custom-select').select2({});
    $('body').on('click', '.js-open-modal', function() {
        // var modal = $(this).data('window');
        $('#modalPhone').addClass('opened');
        $('body').addClass('fixed');
    });
    $('body').on('click', '.modal-close', function() {
        modalClose();
    });
    $('body').on('click', '.modal-overlay', function() {
        modalClose();
    });
    // label click
    $('body').on('change', 'input[id^="agree"]', function() {
        var form = $(this).parents('form');
        var button = form.find('button[type="submit"]');
        if ($(this).is(':checked')) {
            button.attr('disabled',false);
        }
        else {
            button.attr('disabled',true);
        }
    });
    // yandex maps
    ymaps.ready(init);
    function init(){ 
        // Создание карты.    
        var myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 12,
            controls: []
        });

        var myPlacemark1 = new ymaps.Placemark([55.753225, 37.715794], {
            hintContent: 'г. Москва, ул. Авиамоторная, д. 12, оф. 718'
        },
        {
            iconLayout: 'default#image',
            iconImageHref: '../img/marker-map.png',
            iconImageSize: [94, 96]
        });
    
        var myPlacemark2 = new ymaps.Placemark([55.773882, 37.579411], {
            hintContent: 'г. Москва ул. Грузинский Вал, д. 11, стр. 3, под. 3, оф. 19',
        },
        {
            iconLayout: 'default#image',
            iconImageHref: '../img/marker-map.png',
            iconImageSize: [94, 96]
        });
        myMap.geoObjects.add(myPlacemark1);
        myMap.geoObjects.add(myPlacemark2);
    }
    // send small form
    $("form.callback").submit(function(e) {
        e.preventDefault();
        $(this).children('.name').removeClass('error');
        $(this).children('.phone').removeClass('error');
        var flag = true;
        if ( $(this).children('.name').children('input').val() == '' ) {
            $(this).children('.name').addClass('error');
            flag = false;
        }
        if ( $(this).children('.phone').children('input').val() == '' ) {
            $(this).children('.phone').addClass('error');
            flag = false;
        }
        if (flag) {
            var form_data = $(this).serialize();
            console.log(form_data);
            openBigModal();
            $(this).children('.name').children('input').val('');
            $(this).children('.phone').children('input').val('');

            function success(data) {
                openBigModal();
                $(this).children('.name').children('input').val('');
                $(this).children('.phone').children('input').val('');
            }
            $.post("../mail.php", form_data, success);
        }       
    });
});

function modalClose() {
    $('.modal').removeClass('opened');
    $('body').removeClass('fixed');
}

function openBigModal() {
    $('#modalBig').addClass('opened');
    $('body').addClass('fixed');
}
function openModalPrivacy() {
    $('#modal-privacy').addClass('opened');
    $('body').addClass('fixed');
}
function openLastModal() {
    $('#modalLast').addClass('opened');
    $('body').addClass('fixed');
    setTimeout(function() {
        modalClose();
    }, 1500)
}

$(document).ready(function() {
    // calc
    $('input[type=radio]').click(function() {
        switch (event.target.id) {
            case 'ruble-modal':
                    $('#amount-credit-name-modal').html('<i class="fas fa-ruble-sign"></i>');
                break;
            case 'usd-modal':
                    $('#amount-credit-name-modal').html('<i class="fas fa-dollar-sign"></i>');
                break;
            case 'eur-modal':
                    $('#amount-credit-name-modal').html('<i class="fas fa-euro-sign"></i>');
                break;
            default:
                return;
        }
    });
    var workRateModal = $('#work-rate-modal');
    var workRate = $('#work-modal');
    workRateModal.change(function() {
        switch (workRateModal.val()) {
            case '1':
                workRate.children('.l').addClass('white');
                workRate.children('.с').removeClass('white');
                workRate.children('.r').removeClass('white');
                break;
            case '2':
                workRate.children('.l').removeClass('white');
                workRate.children('.с').addClass('white');
                workRate.children('.r').removeClass('white');
                break;
            case '3':
                workRate.children('.l').removeClass('white');
                workRate.children('.с').removeClass('white');
                workRate.children('.r').addClass('white');
                break;
            default:
                return;
        }
    }); 
    var overdueRateModal = $('#overdue-rate-modal');
    var overdue = $('#overdue-modal');
    overdueRateModal.change(function() {
        switch (overdueRateModal.val()) {
            case '1':
                overdue.children('.l').addClass('white');
                overdue.children('.r').removeClass('white');
                break;
            case '2':
                overdue.children('.l').removeClass('white');
                overdue.children('.r').addClass('white');
                break;
            default:
                return;
        }
    }); 
})