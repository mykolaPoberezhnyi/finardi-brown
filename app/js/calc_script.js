$(document).ready(function() {
    $('#amount-credit').on('input', function() {
            $('#amount-credit-input').val($(this).val());
            //$('#amount-credit-input').attr('data-real-summ', $(this).val());
            recalcCredit();
    });
    $('#loan-rate').on('input', function() {
            $('#loan-rate-input').val($(this).val());
            //$('#rate-credit input').attr('data-real-summ', $(this).val());
            recalcCredit();
    });
    $('#credit-term').on('input', function() {
            $('#credit-term-input').val($(this).val());
            recalcCredit();
    });

    $('#amount-credit-input').keyup(function() {
        recalcCredit();
    });
    $('#loan-rate-input').keyup(function() {
        recalcCredit();
    });
    $('#loan-rate-input').on('input', function() {
        $('#loan-rate').val($(this).val());
        recalcCredit();
    });

    $('#credit-term-input').keyup(function() {
        recalcCredit();
    });
    $('#credit-term-input').on('input', function() {
        $('#credit-term').val($(this).val());
        recalcCredit();
    });
    $(".calc input").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and comma
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
    });

    $('div.right-item').click(function(event){
        switch (event.target.id) {
            case 'ruble':
                    $('#amount-credit-name').html('<i class="fas fa-ruble-sign"></i>');
                    $('#credit-min-value').text('150 000');
                    $('#amount-credit').attr('min', 150000);
                    $('#credit-max-value').text('10 000 000');
                    $('#amount-credit').attr('max', 10000000);
                    $('#amount-credit').val(150000);
                    $('.header-calc .currency').html('<i class="fas fa-ruble-sign"></i>');
                break;
            case 'usd':
                    $('#amount-credit-name').html('<i class="fas fa-dollar-sign"></i>');
                    $('#credit-min-value').text('1 000');
                    $('#amount-credit').attr('min', 1000);
                    $('#credit-max-value').text('1 000 000');
                    $('#amount-credit').attr('max', 1000000);
                    $('#amount-credit').val(1000);
                    $('.header-calc .currency').html('<i class="fas fa-dollar-sign"></i>');
                break;
            case 'eur':
                    $('#amount-credit-name').html('<i class="fas fa-euro-sign"></i>');
                    $('#credit-min-value').text('1 000');
                    $('#amount-credit').attr('min', 1000);
                    $('#credit-max-value').text('1 000 000');
                    $('#amount-credit').attr('max', 1000000);
                    $('#amount-credit').val(1000);
                    $('.header-calc .currency').html('<i class="fas fa-euro-sign"></i>');
                break;
            default:
                return;
        }
        $('#amount-credit-input').val($('#amount-credit').val());
        recalcCredit();
    });
    recalcCredit();
    recalcCredit();
});

function number_format( str ){
   return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
}

function recalcCredit() {
    var sum = $("#amount-credit-input").val().replace(/ /g, '');
    var rate = $("#loan-rate-input").val().replace(/ /g, '');
    var term = $("#credit-term-input").val().replace(/ /g, '');
    //console.log(sum, rate, term);
    if (sum!='') {
        $("#amount-credit-input").val(sum);
    }
    if (rate!='') {
        $("#loan-rate-input").val(rate);
    }
    if (term!='') {
        $("#credit-term-input").val(term);
    }
    var res = getResult(sum, term, rate);
    $('#amount-credit-input').val(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    $('#calc .header-calc .value').text(res.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    $('#amount-credit-input-modal').val(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    $('.header-calc .value').maskMoney({thousands: ' '});
}

function getResult(amount, term, rate) {
    var P = parseFloat(toNmb(amount));
    var n = parseFloat(toNmb(term));
    var r = parseFloat(toNmb(rate)) / 1200;

    var result = P * r / (1 - Math.pow((1 + r), - n));

    result = result.toFixed(2);
    result = toStr(result);
    if(result.length > 0 && result != 'NaN') {
        result = result;
    } else {
        result = '0,00';
    }

    return result;
}

function toNmb(str) {
    var nmb = '';
    for (var i=0; i<str.length; i++) {
        n = str[i];
        if(n == ',') {
            nmb += '.';
        } else {
            nmb += n;
        }
    }
    return nmb;
}
function toStr(nmb) {
    var str = '';
    for (var i=0; i<nmb.length; i++) {
        n = nmb[i];
        if(n == '.') {
            str += ',';
        } else {
            str += n;
        }
    }
    return str;
}