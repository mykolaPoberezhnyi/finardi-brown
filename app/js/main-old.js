

const DURATION = 500;

function callMe() {
    //popups = ['.popups', '.submit-your-application'].join(',')
    //toggleForm('.submit-your-application');
    t('.submit-your-application', true);
};

function privacInfo(form){
    //console.log(form);
    close(0, 0);
    t('.privacy-policy', true);
    $('.close').hide();
    $('.comeback').show();
    $('.popups').stop().animate({
      scrollTop: $("body").offset().top
  }, DURATION);
    $('#back-form').val(form);
};

function privacInfo_back(){
    close(0, 0);
    t($('#back-form').val(), true);
    $('#back-form').val('')
    //console.log($('#back-form').val());
    $('.close').show();
    $('.comeback').hide();
};

function showIncreaseForm(id) {
    if ($(".submit-your-application").css("display") !== 'none') {
        close(false, 0);
            $("#callme-form input[type='hidden']").remove();
        t('.increase-your-chances', false);
    } else if (id === 'callme-form-fast' || id === "advice-form") {
        t('.increase-your-chances', true);
    }
    else {
        $(".close").click();
    }
};

function submitCalc(e){
    e.preventDefault();
    callMe();
    const calc_inputs = $("#calc-form").serializeArray();
    $.each(calc_inputs, function(_, data){
        // console.log(data.name, data.value);
        $('#callme-form').append('<input type="hidden" name="' + data.name + '" value="' + data.value + '">');
    });
    $('#callme-form').append('<input type="hidden" name="payment-value" value="' + $('.header-calc .value').text() + '">');
    $('#callme-form').append('<input type="hidden" name="calc" value="1">');
};

function clearForm(id){
    $(id).find("input, textarea")
        .removeClass('error-input')
        .not("input[type='submit'], input[type='radio'], input[type='checkbox']").val("");
    $('.terms-and-conditions input[type="checkbox"]').prop("checked", true);
    $('.btn').prop("disabled", false);
    selectDis();
};

function close(full, d){
    popups = full ? '.popups, .popups>div' : '.popups>div'
    $(popups)
        .animate({
            'opacity': 0,
        }, d, function(){
            $(popups).hide();
            if(full){
                $("html")
                    .removeAttr("style")
                    .css("overflow-y", "scroll");
            }
        });
};

function open(name, full, d){
    popups = full ? ".popups, " + name : name
    if(full){
        $('.popups').show(0);//.css('opacity', 1);
        $("html")
            .css("margin-right", scrollWidth())
            .css("overflow-y", "hidden");
}
    $(popups).show().animate({
        'opacity': 1
    }, d);
};

function t(name, full){
    if($(name).css("display") === "none"){
        open(name, full, DURATION);
        addCloseEvent(name, true);
    }
    else {
        //console.log(`${name} closed`)
        clearForm();
        close(full);
    }
};

function addCloseEvent(name, full) {
    popups = full ? ['.popups', name].join(',') : name

    $(document).unbind('click').on('click', function closePopup(e) {
        if (e.target.className === "popups"){
            //console.log(`document click`);
            //console.log($('#back-form').val(), $('#back-form').val() === '');
            if($('#back-form').val() === '')
                close(1, DURATION);
            else
                privacInfo_back();
            clearForm(name)
        }
        e.stopPropagation();
    });
    $(name + " > .close").unbind('click').click(function() {
        //console.log(`${name} > .close click`)
        close(1, DURATION);
        clearForm(name)
    });
};


var submitAction = function (e) {
    e.preventDefault();
    var frm = $(this);
    $('form#increase-form  #phone').val(
        frm.find('input[name="phone"]').val()
    );
    //console.log(frm.find('input[name="username"]').val());
    $('form#increase-form  #username').val(
        frm.find('input[name="username"]').val()
    );
    frm.find('input, textarea').removeClass('error-input');
    //var urlParams = new URLSearchParams(window.location.search);
    let getQuery = window.location.search.replace(/\?/g, '').replace(/&/g, ':');

    var aduid_value = "";
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + "_aid".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    aduid_value = matches ? decodeURIComponent(matches[1]) : "";
    $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize() + '&utm=' + getQuery + '&aduid=' + aduid_value,
            success: function (data) {
                showIncreaseForm(frm.attr('id'));
                clearForm(frm);
            },
            error: function (data) {
                var json = JSON.parse(data.responseText);
                var errors = json['errors'];
                console.log(errors);
                $.each(errors, function( _, value ) {
                        $(frm).find("input[name='" + value + "'], textarea[name='" + value + "'], select[name='" + value + "']")
                            .addClass('error-input');
                    });
            },
        });
    };

function selectDis(){
    //$('.vert input[type="checkbox"]').prop( "checked", false );
    $('select option:contains("Выберите регион")').prop('selected', true);
    // $('.vert select').addClass('disabled');
}

$(document).ready(function() {

var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + "_aid".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  aduid_value = matches ? decodeURIComponent(matches[1]) : undefined;
  
    console.log(aduid_value);
    $("input[placeholder='+7 (___) ___-__-__'], input[type='tel']").phoneMask("+7 (999) 999-99-99", {placeholder: "+7 (___) ___-__-__"});
    if ( $(window).width() < 768 ) {
        $("input[placeholder='+7 (___) ___-__-__'], input[type='tel']").attr("type", "tel");
        $("input[placeholder='+7 (___) ___-__-__'], input[type='tel']").phoneMask("+7 (999) 999-99-99", {placeholder: "+7 (___) ___-__-__"});         
            
    }
    $('a').each(function(_, item) {
        if ($(item).attr('href') === '#')
            $(item).attr('href', 'javascript:void(0);');
    });
    $("a[href*=\\#]").on("click", function(e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, DURATION);
        e.preventDefault();
        return false;
    });
    var forms = [
        '#callme-form',
        '#review-form',
        '#callme-form-fast',
        '#advice-form',
        '#increase-form'
    ]
    $(forms.join(',')).bind('submit', submitAction);
    $('#calc-form').bind('submit', submitCalc);

    //$('.terms-and-conditions #p-i-agree').prop('checked', true);
    $('.increase-your-chances .btn').prop('disabled', false);

    $('.terms-and-conditions input[type="checkbox"]').not('#p-i-agree-n').change(function() {
        //console.log($(this).parent('div'));
            var popup = $(this).parent('div').parent('div').attr('class').split(' ').join('.');
        if($(this).is(':checked')){
            //console.log(this.id);
            $(this).parent().parent().find('.btn').prop('disabled', false);
            // $(`.terms-and-conditions #${this.id}`).parent().children('.t').html(
            //     `<label for="${this.id}">Я даю согласие ООО «Финарди» <br> на обработку</label>
            //     <a href="javascript:privacInfo('.${popup}')"> моих персональных данных</a>`
            // );
        }else{
            // $(`.terms-and-conditions #${this.id}`).parent().children('.t').html(
            //     `<label for="${this.id}">Вы должны дать свое согласие на</label>
            //     <a href="javascript:privacInfo('.${popup}')"> обработку ваших персональных данных</a>`
            // );
            // $('.terms-and-conditions .t').html(
            //     `<label for="${this.id}">Вы должны дать свое согласие на</label>
            //     <a href="javascript:privacInfo()"> обработку ваших персональных данных</a>`
            // );
            $(this).parent().parent().find('.btn').prop('disabled', true);
        }
    });

    $('input#p-i-agree-n').change(function() {
            //var popup = $(this).parent('div').parent('div').attr('class').split(' ').join('.');
            var button = $(this).parent('div').parent('form').find('.btn');
        if($(this).is(':checked')){
            button.prop('disabled', false);
            // $(this).children('.t').html(
            //     `<label for="#p-i-agree-n">Я даю согласие ООО «Финарди» <br> на обработку</label>
            //     <a href="javascript:t('.privacy-policy')"> моих персональных данных</a>`
            // );
        }else{
            // $(this).children('.t').html(
            //     `<label for="#p-i-agree-n">Вы должны дать свое согласие на</label>
            //     <a href="javascript:t('.privacy-policy')"> обработку ваших персональных данных</a>`
            // );
            button.prop('disabled', true);
        }
    });
    $(".all-reviews").click(function() {
        $("#other-reviews").animate({
            height: 'hide'
        }, 500);
        if ($("#other-reviews").css('display') == 'none') {
            $("#other-reviews").animate({
                height: 'show'
            }, 500);
            $(".all-reviews .arrow").css('transform', 'rotate(180deg)');
        } else {
            $("#other-reviews").animate({
                height: 'hide'
            }, 500);
            $(".all-reviews .arrow").css('transform', '');
        }
    });

    $('.own-label label').click(function(){
        let select = $(this).parent('div').find('select');
        if(select.hasClass('disabled')){
            $(select).removeClass('disabled');
        }
        else{
            $(select).addClass('disabled');
        }
    });


    $('.dropdown select').on('mousedown', function(e){
        if($(this).hasClass('disabled')){
            //e.preventDefault();
            $(this)
                .parent('div')
                .parent('div')
                .find('input').prop('checked', true);
            $(this).removeClass('disabled');
        }
    });
    //selectDis();

    handleadmitadUid();

});

function handleadmitadUid(lifeTime) {
    var aid = (/admitad_uid=([^&]+)/.exec(location.search) || [])[1];
    if (!aid) {
        return;
    }

    var today =new Date();

    var expiresDate = new Date(today.getTime() + 3600 * 24 * 90 * 1000);
    var cookieString = '_aid=' + aid + '; path=/; expires=' + expiresDate + ';';
    document.cookie = cookieString;
    document.cookie = cookieString + '; domain=.' + location.host;
}

function scrollWidth(){
    var div = document.createElement('div');
    div.style.overflowY = 'scroll';
    div.style.width = '50px';
    div.style.height = '50px';
    div.style.visibility = 'hidden';
    document.body.appendChild(div);
    var scrollWidth = div.offsetWidth - div.clientWidth;
    document.body.removeChild(div);
    return scrollWidth;
};


$(document).ready(function() {
    var video = document.getElementById("video");
    var playButton = $(".video-play-in-modal-w");

    playButton.click(function() {
        video.play();
        playpause();
    })
    playpause();

    $("#video").click(function() {      
        video.paused ? video.play() : video.pause();
        playpause();
    });
    // modal-w
    $("#video-play").click(function(event) {
        event.preventDefault();
        $('body').addClass('modal-w-show');
        $("#modal-w").addClass('show');
        playButton.css("display", "block")
    });
    $(".modal-w__close").click(function() {
        video.pause()
        closemodal();
    });
    $(".modal-w__overlay").click(function() {
        video.pause()
        closemodal();
    });
})

function playpause() {
    var video = document.getElementById("video");
    var playButton = $(".video-play-in-modal-w");
    video.paused ? playButton.css("display", "block") : playButton.css("display", "none");
}

function closemodal() {
    $('body').removeClass('modal-w-show');
    $("#modal-w").removeClass('show');

}
