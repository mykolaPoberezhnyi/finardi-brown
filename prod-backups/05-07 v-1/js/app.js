$(document).ready(function() {

	$('#amount-credit').on('input', function() {
		$('#amount-credit-input').val($(this).val());
		//$('#amount-credit-input').attr('data-real-summ', $(this).val());
		recalcCredit();
	});
	$('#loan-rate').on('input', function() {
		$('#loan-rate-input').val($(this).val());
		//$('#rate-credit input').attr('data-real-summ', $(this).val());
		recalcCredit();
	});
	$('#credit-term').on('input', function() {
		$('#credit-term-input').val($(this).val());
		recalcCredit();
	});

	$('#amount-credit-input').keyup(function() {
		recalcCredit();
	});
	$('#loan-rate-input').keyup(function() {
		recalcCredit();
	});
	$('#loan-rate-input').on('input', function() {
		$('#loan-rate').val($(this).val());
		recalcCredit();
	});

	$('#credit-term-input').keyup(function() {
		recalcCredit();
	});
	$('#credit-term-input').on('input', function() {
		$('#credit-term').val($(this).val());
		recalcCredit();
	});
	$('.calc input').keydown(function(e) {
		// Allow: backspace, delete, tab, escape, enter and comma
		if (
			$.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188]) !== -1 ||
			// Allow: Ctrl+A
			(e.keyCode == 65 && e.ctrlKey === true) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)
		) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('div.right-item').click(function(event) {
		//console.log(event.target.id.trim());
		switch (event.target.id) {
			case 'ruble':
				$('#amount-credit-name').html('<i class="fas fa-ruble-sign"></i>');
				$('#credit-min-value').text('150 000');
				$('#amount-credit').attr('min', 150000);
				$('#credit-max-value').text('10 000 000');
				$('#amount-credit').attr('max', 10000000);
				$('#amount-credit').val(150000);
				$('.header-calc .currency').html('<i class="fas fa-ruble-sign"></i>');

				break;
			case 'usd':
				$('#amount-credit-name').html('<i class="fas fa-dollar-sign"></i>');
				$('#credit-min-value').text('1 000');
				$('#amount-credit').attr('min', 1000);
				$('#credit-max-value').text('1 000 000');
				$('#amount-credit').attr('max', 1000000);
				$('#amount-credit').val(1000);
				$('.header-calc .currency').html('<i class="fas fa-dollar-sign"></i>');
				break;
			case 'eur':
				$('#amount-credit-name').html('<i class="fas fa-euro-sign"></i>');
				$('#credit-min-value').text('1 000');
				$('#amount-credit').attr('min', 1000);
				$('#credit-max-value').text('1 000 000');
				$('#amount-credit').attr('max', 1000000);
				$('#amount-credit').val(1000);
				$('.header-calc .currency').html('<i class="fas fa-euro-sign"></i>');
				break;
			default:
				return;
		}
		$('#amount-credit-input').val($('#amount-credit').val());
		recalcCredit();
	});
	recalcCredit();
	recalcCredit();

	function number_format(str) {
		return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
	}

	function recalcCredit() {
		if (
			$('#amount-credit-input').val() &&
			$('#loan-rate-input').val() &&
			$('#credit-term-input').val()
		) {
			var sum = $('#amount-credit-input')
				.val()
				.replace(/ /g, '');
			var rate = $('#loan-rate-input')
				.val()
				.replace(/ /g, '');
			var term = $('#credit-term-input')
				.val()
				.replace(/ /g, '');
			//console.log(sum, rate, term);
			if (sum != '') {
				$('#amount-credit-input').val(sum);
			}
			if (rate != '') {
				$('#loan-rate-input').val(rate);
			}
			if (term != '') {
				$('#credit-term-input').val(term);
			}
			var res = getResult(sum, term, rate);
			$('#amount-credit-input').val(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
			$('.header-calc .value').text(res.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
			$('.header-calc .value').maskMoney({ thousands: ' ' });
		}
	}

	function getResult(amount, term, rate) {
		var P = parseFloat(toNmb(amount));
		var n = parseFloat(toNmb(term));
		var r = parseFloat(toNmb(rate)) / 1200;

		var result = P * r / (1 - Math.pow(1 + r, -n));

		result = result.toFixed(2);
		result = toStr(result);
		if (result.length > 0 && result != 'NaN') {
			result = result;
		} else {
			result = '0,00';
		}

		return result;
	}

	function toNmb(str) {
		var nmb = '';
		for (var i = 0; i < str.length; i++) {
			n = str[i];
			if (n == ',') {
				nmb += '.';
			} else {
				nmb += n;
			}
		}
		return nmb;
	}
	function toStr(nmb) {
		var str = '';
		for (var i = 0; i < nmb.length; i++) {
			n = nmb[i];
			if (n == '.') {
				str += ',';
			} else {
				str += n;
			}
		}
		return str;
	}

	$(function() {
		$('.trigger-modal').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#username',
			mainClass: 'trigger'
		});
		$(document).on('click', '.modal-close', function(e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	});

	$(function() {
		$('.trigger-modal-calc').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#username',
			prependTo: document.getElementById('append-form-container')
		});
		// $(document).on('click', '.modal-close', function(e) {
		// 	e.preventDefault();
		// 	$.magnificPopup.close();
		// });
	});

	$(function() {
		$('.additionally-info').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#username'
		});
		$(document).on('click', '.modal-close', function(e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	});

	$('.second-popup-form-trigger, .privacy-popup').magnificPopup({
		closeBtnInside: true
	});

	$('.form-default').submit(function() {
		//Change
		var th = $(this);
		let getQuery = window.location.search.replace(/\?/g, '').replace(/&/g, ':');

		var payment_value = $('.header-calc .value').text();
		var aduid_value = '';
		var matches = document.cookie.match(
			new RegExp('(?:^|; )' + '_aid'.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)')
		);
		aduid_value = matches ? decodeURIComponent(matches[1]) : '';
		$.ajax({
			type: 'POST',
			url: '/callme',
			data:
				th.serialize() +
				'&utm=' +
				getQuery +
				'&aduid=' +
				aduid_value +
				'&payment-value=' +
				payment_value
		}).done(function() {
			var arrForNextForm = th.serializeArray();
			// console.log(arrForNextForm);
			$(arrForNextForm).each(function(i, e) {
				$('#additionally-info input[name=' + e.name + ']').attr('value', e.value);
			});
			setTimeout(function() {
				$.magnificPopup.open({
					items: {
						src: '#additionally-info'
					},
					type: 'inline',
					preloader: false,
					focus: '#username'
				});
				th.trigger('reset');
			}, 500);
		});
		return false;
	});

	$('.additionally-send').submit(function() {
		//Change
		var th = $(this);
		let getQuery = window.location.search.replace(/\?/g, '').replace(/&/g, ':');

		var aduid_value = '';
		var matches = document.cookie.match(
			new RegExp('(?:^|; )' + '_aid'.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)')
		);
		aduid_value = matches ? decodeURIComponent(matches[1]) : '';
		$.ajax({
			type: 'POST',
			url: '/increase', //Change
			data: th.serialize() + '&utm=' + getQuery + '&aduid=' + aduid_value
		}).done(function() {
			setTimeout(function() {
				$.magnificPopup.open({
					items: {
						src: '#thanks'
					},
					type: 'inline',
					preloader: false,
					focus: '#username',
					modal: true
				});
				setTimeout(function() {
					$.magnificPopup.close();
				}, 4000);
				th.trigger('reset');
			}, 500);
		});
		return false;
	});

	$(document).ready(function() {
		$('body').on('click', 'a', function(event) {
			var id = $(this).attr('href'),
				top = $(id).offset().top;
			$('body,html').animate({ scrollTop: top }, 900);
		});
	});

	$('.action--open').click(function() {
		$('.header__menu').addClass('toggle-menu_open');
	});

	$('.action--close').click(function() {
		$('.header__menu').removeClass('toggle-menu_open');
	});

	$('.header__menu a').click(function() {
		$('.action--close').trigger('click');
	});

	// yandex maps - contacts page
	$(function() {
		ymaps.ready(init);
		var myMap, myPlacemark, zoom;

		if (screen.width < 768) {
			zoom = 11;
		} else {
			zoom = 13;
		}

		function init() {
			myMap = new ymaps.Map('map', {
				center: [55.7636, 37.649218],
				zoom: zoom
			});

			myMap.controls.remove('geolocationControl');
			myMap.controls.remove('searchControl');
			myMap.controls.remove('routeButtonControl');
			myMap.controls.remove('trafficControl');
			myMap.controls.remove('typeSelector');
			myMap.controls.remove('fullscreenControl');
			myMap.controls.remove('rulerControl');

			myMap.behaviors.disable('scrollZoom');

			(myPlacemark = new ymaps.Placemark(
				[55.75322, 37.715471],
				{
					hintContent: '105037, Москва, ул. Авиамоторная, 12, оф. 718'
				},
				{
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: 'default#image',
					// Своё изображение иконки метки.
					iconImageHref: 'img/marker-map.png',
					// Размеры метки.
					iconImageSize: [180, 45],
					iconImageOffset: [-90, -45]
				}
			)),
				myMap.geoObjects.add(myPlacemark);

			(myPlacemark = new ymaps.Placemark(
				[55.774289068964684, 37.57898050000001],
				{
					hintContent: '123056, ул. Грузинский Вал, д.11, стр. 3, оф.19'
				},
				{
					// Опции.
					// Необходимо указать данный тип макета.
					iconLayout: 'default#image',
					// Своё изображение иконки метки.
					iconImageHref: 'img/marker-map.png',
					// Размеры метки.
					iconImageSize: [180, 45],
					iconImageOffset: [-90, -45]
				}
			)),
				myMap.geoObjects.add(myPlacemark);
		}
	});

	jQuery(function($) {
		$("input[name='phone']").mask('+7 (999) 999-99-99');
	});

	$('select').niceSelect();

	$('input[name="price"]').on('input', function(e) {
		var str = String($(this).val()).replace(/\s/gi, '');
		$(this).val(str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
	});

	$('.custom-radio input[type="checkbox"]').change(function(e) {
		var valCurCheckbox = e.target.attributes.dataname.value;
		//$(this).prop('checked', true);
		var checkThis = $(this);
		// console.log(this);

		$('.additionally-info_item--checkboxes select ').each(function(i, elem) {
			if ($(elem).attr('name') == valCurCheckbox && e.target.checked) {
				$(elem)
					.next()
					.removeClass('disable');
			}
			if ($(elem).attr('name') == valCurCheckbox && !e.target.checked) {
				$(elem)
					.next()
					.addClass('disable');
				$(elem).val('');
				$(elem)
					.next()
					.click(function() {
						$(this).removeClass('disable');
						e.target.checked = true;
						// console.log(valCurCheckbox);
					});
				if (e.target.checked == false) {
					$(elem).val('');
				}
			}
		});
	});

	//disable send-btn
	if ($('.agreement__checkbox input'))
		$('.agreement__checkbox input').change(function(e) {
			var btn = $(e.target.parentNode.parentNode.previousSibling.previousSibling);
			console.log(btn);
			if ($(this).is(':checked')) {
				btn.removeClass('disabled');
				return;
			}
			btn.addClass('disabled');
		});
});

// function initMap() {
// 	// Snazzy Map Style
// 	var mapStyle = [
// 		{
// 			featureType: 'all',
// 			elementType: 'labels.text',
// 			stylers: [{ visibility: 'off' }]
// 		},
// 		{
// 			featureType: 'all',
// 			elementType: 'labels.text.fill',
// 			stylers: [{ saturation: 36 }, { color: '#333333' }, { lightness: 40 }, { visibility: 'off' }]
// 		},
// 		{
// 			featureType: 'all',
// 			elementType: 'labels.text.stroke',
// 			stylers: [{ visibility: 'on' }, { color: '#c2c2c2' }, { lightness: 16 }]
// 		},
// 		{
// 			featureType: 'all',
// 			elementType: 'labels.icon',
// 			stylers: [{ visibility: 'off' }]
// 		},
// 		{
// 			featureType: 'administrative',
// 			elementType: 'geometry.fill',
// 			stylers: [{ color: '#fefefe' }, { lightness: 20 }]
// 		},
// 		{
// 			featureType: 'administrative',
// 			elementType: 'geometry.stroke',
// 			stylers: [{ color: '#fefefe' }, { lightness: 17 }, { weight: 1.2 }]
// 		},
// 		{
// 			featureType: 'landscape',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#f5f5f5' }, { lightness: 20 }]
// 		},
// 		{
// 			featureType: 'poi',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#f5f5f5' }, { lightness: 21 }]
// 		},
// 		{
// 			featureType: 'poi.park',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#dedede' }, { lightness: 21 }]
// 		},
// 		{
// 			featureType: 'road.highway',
// 			elementType: 'geometry.fill',
// 			stylers: [{ color: '#ffffff' }, { lightness: 17 }]
// 		},
// 		{
// 			featureType: 'road.highway',
// 			elementType: 'geometry.stroke',
// 			stylers: [{ color: '#ffffff' }, { lightness: 29 }, { weight: 0.2 }]
// 		},
// 		{
// 			featureType: 'road.arterial',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#ffffff' }, { lightness: 18 }]
// 		},
// 		{
// 			featureType: 'road.local',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#ffffff' }, { lightness: 16 }]
// 		},
// 		{
// 			featureType: 'transit',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#f2f2f2' }, { lightness: 19 }]
// 		},
// 		{
// 			featureType: 'water',
// 			elementType: 'geometry',
// 			stylers: [{ color: '#e9e9e9' }, { lightness: 17 }]
// 		}
// 	];

// 	// Create the map
// 	var map = new google.maps.Map(document.getElementById('map'), {
// 		zoom: 17,
// 		styles: mapStyle,
// 		center: new google.maps.LatLng(55.753231, 37.715666),
// 		mapTypeControl: false,
// 		fullscreenControlOptions: {
// 			position: google.maps.ControlPosition.BOTTOM_LEFT
// 		}
// 	});

// 	// Add a marker
// 	var marker = new google.maps.Marker({
// 		map: map,
// 		position: new google.maps.LatLng(55.753231, 37.715666),
// 		icon: {
// 			url: 'img/marker-map.svg',
// 			size: new google.maps.Size(180, 45)
// 		}
// 	});
// }

// initMap();

//animation canvas

// function wave_1(background, canvas_id, max_height, wave_compression) {
// 	var WIDTH = $(window).width();
// 	var height = 400;
// 	var background = background;

// 	function sketchProc(processing) {
// 		processing.draw = function() {
// 			processing.background(0, 0, 0, 0);
// 			var now = +new Date();
// 			var dt = now - lastUpdate;
// 			lastUpdate = now;
// 			update(dt);
// 			draw();
// 		};
// 		processing.setup = function() {
// 			processing.size(WIDTH, height);
// 			processing.frameRate(60);
// 		};
// 	}
// 	var canvas = document.getElementById(canvas_id);
// 	// attaching the sketchProc function to the canvas
// 	var lastUpdate = +new Date();
// 	var pjs = new Processing(canvas, sketchProc);

// 	// Resolution of simulation
// 	var NUM_POINTS = 200;
// 	// Width of simulation
// 	var Y_OFFSET = 240;
// 	var ITERATIONS = 500;
// 	// Make points to go on the wave
// 	function makeWavePoints(numPoints) {
// 		var t = [];
// 		for (var n = 0; n < numPoints + 3; n++) {
// 			// This represents a point on the wave
// 			var newPoint = {
// 				x: n / numPoints * WIDTH,
// 				y: Y_OFFSET
// 			};
// 			t.push(newPoint);
// 		}
// 		return t;
// 	}

// 	// A phase difference to apply to each sine
// 	var offset = 0;

// 	var NUM_BACKGROUND_WAVES = 25;
// 	var BACKGROUND_WAVE_MAX_HEIGHT = max_height;
// 	var BACKGROUND_WAVE_COMPRESSION = wave_compression;
// 	// Amounts by which a particular sine is offset
// 	var sineOffsets = [];
// 	// Amounts by which a particular sine is amplified
// 	var sineAmplitudes = [];
// 	// Amounts by which a particular sine is stretched
// 	var sineStretches = [];
// 	// Amounts by which a particular sine's offset is multiplied
// 	var offsetStretches = [];
// 	// Set each sine's values to a reasonable random value
// 	for (var i = -0; i < NUM_BACKGROUND_WAVES; i++) {
// 		var sineOffset = -Math.PI + 2 * Math.PI * Math.random();
// 		sineOffsets.push(sineOffset);
// 		var sineAmplitude = Math.random() * BACKGROUND_WAVE_MAX_HEIGHT;
// 		sineAmplitudes.push(sineAmplitude);
// 		var sineStretch = Math.random() * BACKGROUND_WAVE_COMPRESSION;
// 		sineStretches.push(sineStretch);
// 		var offsetStretch = Math.random() * BACKGROUND_WAVE_COMPRESSION;
// 		offsetStretches.push(offsetStretch);
// 	}

// 	// This function sums together the sines generated above,
// 	// given an input value x
// 	function overlapSines(x) {
// 		var result = 0;
// 		for (var i = 0; i < NUM_BACKGROUND_WAVES; i++) {
// 			result =
// 				result +
// 				sineOffsets[i] +
// 				sineAmplitudes[i] * Math.sin(x * sineStretches[i] + offset * offsetStretches[i]);
// 		}
// 		return result;
// 	}

// 	var wavePoints = makeWavePoints(NUM_POINTS);

// 	// Callback when updating
// 	function update(dt) {
// 		offset = offset + 1;
// 	}

// 	// Callback for drawing
// 	function draw() {
// 		var canvas = document.getElementById(canvas_id);
// 		var context = canvas.getContext('2d');

// 		var firstPoint = wavePoints[0];
// 		var lastPoint = wavePoints[wavePoints.length - 1];

// 		context.beginPath();
// 		context.moveTo(firstPoint.x, firstPoint.y + overlapSines(firstPoint.x));
// 		for (var n = 0; n < wavePoints.length; n++) {
// 			var p = wavePoints[n];
// 			if (n > 0) {
// 				var leftPoint = wavePoints[n - 1];
// 				context.lineTo(p.x, p.y + overlapSines(p.x));
// 			}
// 		}

// 		var canvasHeight = $('#' + canvas_id).height();
// 		context.lineTo(lastPoint.x, canvasHeight);
// 		context.lineTo(0, canvasHeight);
// 		context.lineTo(0, firstPoint.y + overlapSines(firstPoint.x));

// 		context.closePath();

// 		context.lineWidth = 0;
// 		context.strokeStyle = background;
// 		context.fillStyle = background;
// 		context.fill();
// 		context.stroke();
// 	}
// }

// wave_1('#4181F9', 'wave_1', 10, 1 / 22);
// wave_1('#4181F9', 'wave_2', 10, 1 / 25);
